export class IPokemon {
    name: string;
    id: number;
    height: number;
    weight: number;
    type: string;
    moves: Array<string>;
    captured: boolean;
    nick: string;

    constructor(name: string, id: number, height: number,
            weight: number, type: string, moves: Array<string>) {
        this.name = name;
        this.id = id;
        this.height = height;
        this.weight = weight;
        this.type = type;
        this.moves = moves;
    }
}