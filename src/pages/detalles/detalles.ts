import { Component, Input } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { IPokemon } from '../../pokemon/IPokemon';
import { NativeStorage } from '@ionic-native/native-storage';

/**
 * Generated class for the DetallesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-detalles',
  templateUrl: 'detalles.html'
})
export class DetallesPage {
  pokemon: IPokemon;
  image: string;
  cardColor: string;
  width: string;
  typeImage: string;
  moves: Array<any> = [];
  @Input('apodo') apodo: string = '';

  constructor(public nativeStorage: NativeStorage, public navCtrl: NavController, public navParams: NavParams, public platform: Platform) {
    this.pokemon = this.navParams.get('pokemon');
    this.image = this.navParams.get('image');

    let length: number = 2;
    if(this.pokemon.moves.length < 2) {
      length = this.pokemon.moves.length;
    }

    for(let i = 0; i < length; i++) {
      this.moves.push({move: this.pokemon.moves[i], power: this.getRandom()});
    }

    if(this.pokemon.nick) {
      this.apodo = this.pokemon.nick
    }
    
    if(!this.platform.is('mobile')) {
      this.width = '450px';
    }
    
    switch (this.pokemon.type) {
      case 'grass':
        this.cardColor = 'rgb(105, 194, 105)';
        this.typeImage = 'hoja';
        break;
      case 'fire':
        this.cardColor = 'red';
        this.typeImage = 'fuego';
        break;
      case 'water':
        this.cardColor = 'blue';
        this.typeImage = 'agua';
        break;
      case 'bug':
        this.cardColor = 'olive';
        this.typeImage = 'bicho';
        break;
      case 'normal':
        this.cardColor = 'chocolate';
        this.typeImage = 'default';
        break;
      default:
        this.cardColor = 'beige';
        this.typeImage = 'default';
        break;
    }
  }

  ionViewDidLoad() {
    
  }

  getRandom() {
    return Math.floor(Math.random() * (11 - 1) + 1) * 10;
  }

  liberar() {
    let self = this;
    this.pokemon.captured = false;
    this.nativeStorage.getItem('pokedex').then(
      data => {
        let pokedex: Array<any> = data.filter(function(el) {
          return el.id != self.pokemon.id;
        });
        self.pokemon.nick = undefined;
        self.apodo = '';
        self.nativeStorage.setItem('pokedex', pokedex).then(() => console.log('stored item'));
      },
      error => {
        this.nativeStorage.setItem('pokedex', []).then(() => console.log('stored item'));
      }
    );
  }

  capturar() {
    let self = this;
    this.pokemon.captured = true;
    this.nativeStorage.getItem('pokedex').then(
      data => {
        let pokedex: Array<any> = data;
        pokedex.push({id: self.pokemon.id});
        self.nativeStorage.setItem('pokedex', pokedex).then(() => console.log('stored item'));
      },
      error => {
        this.nativeStorage.setItem('pokedex', []).then(() => console.log('stored item'));
      }
    );
  }

  ponerApodo() {
    let self = this;
    self.nativeStorage.getItem('pokedex').then(
      data => {
        let pokedex: Array<any> = data.filter(function(el) {
          return el.id != self.pokemon.id;
        });
        pokedex.push({id: self.pokemon.id, nick: self.apodo});
        self.pokemon.nick = self.apodo;
        self.nativeStorage.setItem('pokedex', pokedex).then(() => alert('Apodo establecido'));
      },
      error => {
        self.nativeStorage.setItem('pokedex', []).then(() => console.log('stored item'));
      }
    );
  }

  escribirApodo(event: any) {
    console.log(event);
    if(event.target.value.length > 5) {
      event.target.value = event.target.value.substr(0, 5);
      this.apodo = event.target.value.substr(0, 5);
    }
  }

}
