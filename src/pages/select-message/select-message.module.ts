import { NgModule } from '@angular/core';
import { IonicPageModule, IonicModule } from 'ionic-angular';
import { SelectMessagePage } from './select-message';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

@NgModule({
  declarations: [
    SelectMessagePage,
  ],
  imports: [
    IonicModule,
    FormsModule,
    CommonModule,
    IonicPageModule.forChild(SelectMessagePage),
  ],
})
export class SelectMessagePageModule {}
