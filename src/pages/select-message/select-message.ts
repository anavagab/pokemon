import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';

/**
 * Generated class for the SelectMessagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-select-message',
  templateUrl: 'select-message.html',
})
export class SelectMessagePage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform) {
    
    navCtrl.getPrevious()
  }

  ionViewDidLoad() {
  }

  back() {
    this.navCtrl.pop();
  }
}
