import { Component } from '@angular/core';
import { NavController, Platform } from 'ionic-angular';
import { SecondPage } from '../second/second';
import { ExampleApiProvider } from '../../providers/example-api/example-api';
import { IPokemon } from '../../pokemon/IPokemon';
import { HTTP } from '@ionic-native/http/ngx';
import { NativeStorage } from '@ionic-native/native-storage';
import { TraductorPipe } from '../../pipes/traductor/traductor';
import { SessionProvider } from '../../providers/session/session';
import { LoginPage } from '../login/login';
import { SelectMessagePage } from '../select-message/select-message';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  exampleText: string = "primera parte";
  fromApi: Array<IPokemon> = [];
  lang: string = 'es';
  pokedex: Array<any>;

  constructor(public navCtrl: NavController,
      public exampleApiProvider: ExampleApiProvider,
      public http: HTTP,
      public nativeStorage: NativeStorage,
      public traductor: TraductorPipe,
      public sessionProvider: SessionProvider,
      public platform: Platform) {
    
    platform.ready().then(() => {
      if(!platform.is('core'))
        this.sessionProvider.getLanguage()
          .then((response) => {
            this.lang = response as string;
          });
    })
    
  }

  ionViewDidLoad(){
    this.platform.ready().then(() => {
      this.exampleApiProvider.loadPokemons2()
      .then(data => {
        data['results'].forEach(p => {
          this.exampleApiProvider.loadPokemonsDetails(p['url'])
            .then(p1 => {
              let moves: Array<string> = [];
              p1['moves'].forEach(move => {
                moves.push(move['move']['name']);
              });
              let pokemon = new IPokemon(p1['name'], p1['id'], p1['height'],
                p1['weight'], p1['types'][0]['type']['name'], moves);
              
              if(!this.pokedex) {
                this.nativeStorage.getItem('pokedex').then(
                  data => {
                    this.pokedex = data;
                    this.pokedex.forEach((poked) => {
                      if(pokemon.id == poked.id) {
                        pokemon.captured = true;
                        if(poked.nick) {
                          pokemon.nick = poked.nick;
                        }
                      }
                    });
                    this.fromApi.push(pokemon);
                  },
                  error => {
                    this.nativeStorage.setItem('pokedex', []).then(() => console.log('stored item'));
                    this.fromApi.push(pokemon);
                  }
                );
              }
              else {
                this.pokedex.forEach((poked) => {
                  if(pokemon.id == poked.id) {
                    pokemon.captured = true;
                  }
                });
                this.fromApi.push(pokemon);
              }
              
              
              this.fromApi.sort((a, b) => {
                if(a['id'] > b['id']) {
                  return 1;
                }
                return -1;
              });
            });
        })
      });
    })  
  }

  ejercicio() {
    this.exampleText = "segunda parte";

    this.navCtrl.push(SecondPage, {
      param: "desde navegación"
    });
  }

  traducir() {
    if(this.lang == 'es') {
      this.lang = 'en';
      this.sessionProvider.setLanguage('en');
    }
    else {
      this.lang = 'es';
      this.sessionProvider.setLanguage('es');
    }
  }

  login() {
    this.navCtrl.push(LoginPage, {});
  }

  message() {
    this.navCtrl.push(SelectMessagePage, {});
  }

}
