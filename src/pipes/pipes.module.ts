import { NgModule } from '@angular/core';
import { TraductorPipe } from './traductor/traductor';
@NgModule({
	declarations: [TraductorPipe],
	imports: [],
	exports: [TraductorPipe]
})
export class PipesModule {}
