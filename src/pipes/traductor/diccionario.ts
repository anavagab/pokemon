export const DICCIONARIO = {
    es: {
        title: 'Listado pokemon',
        translate: 'Traducir',
        button: 'Botón'
    },
    en: {
        title: 'Pokemon list',
        translate: 'Translate',
        button: 'Button'
    }
}