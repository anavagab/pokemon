import { Pipe, PipeTransform } from '@angular/core';
import { DICCIONARIO } from './diccionario';

/**
 * Generated class for the TraductorPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'traductor',
})
export class TraductorPipe implements PipeTransform {
  
  transform(value: string, lang: string) {
    return DICCIONARIO[lang][value];
  }
}
