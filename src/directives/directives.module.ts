import { NgModule } from '@angular/core';
import { MaxLengthDirective } from './max-length/max-length';
@NgModule({
	declarations: [MaxLengthDirective],
	imports: [],
	exports: [MaxLengthDirective]
})
export class DirectivesModule {}
