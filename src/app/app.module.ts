import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule, IonicPageModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { SecondPage } from '../pages/second/second';
import { LoginPage } from '../pages/login/login';
import { SelectMessagePage } from '../pages/select-message/select-message';
import { ExampleApiProvider } from '../providers/example-api/example-api';
import { HttpClientModule } from '@angular/common/http';
import { ListarPokemonsComponent } from '../components/listar-pokemons/listar-pokemons';
import { HTTP } from '@ionic-native/http/ngx';
import { NativeStorage } from '@ionic-native/native-storage'
import { TraductorPipe } from '../pipes/traductor/traductor';
import { SessionProvider } from '../providers/session/session';
import { DetallesPage } from '../pages/detalles/detalles';
import { MaxLengthDirective } from '../directives/max-length/max-length';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    SecondPage,
    ListarPokemonsComponent,
    TraductorPipe,
    DetallesPage,
    MaxLengthDirective,
    LoginPage,
    SelectMessagePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    IonicPageModule.forChild(DetallesPage)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    SecondPage,
    DetallesPage,
    LoginPage,
    SelectMessagePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HTTP,
    ExampleApiProvider,
    NativeStorage,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TraductorPipe,
    SessionProvider
  ]
})
export class AppModule {}
