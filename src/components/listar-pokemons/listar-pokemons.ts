import { Component, Input } from '@angular/core';
import { IPokemon } from '../../pokemon/IPokemon';
import { ImagePickerOptions } from '@ionic-native/image-picker/ngx';
import { NativeStorage } from '@ionic-native/native-storage';
import { NavController, Platform } from 'ionic-angular';
import { DetallesPage } from '../../pages/detalles/detalles';

declare var window: any;

/**
 * Generated class for the ListarPokemonsComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'listar-pokemons',
  templateUrl: 'listar-pokemons.html'
})
export class ListarPokemonsComponent {

  @Input() pokemons: Array<IPokemon>;
  images: Array<IImage> = [];
  pokedex: Array<any> = [];
  isBrowser: boolean = !this.platform.is('mobile');
  public imagePath;
  imgURL: Array<any> = [];

  constructor(public nativeStorage: NativeStorage,
      public navCtrl: NavController, public platform: Platform) {

    this.platform.ready().then(() => {
      if(this.platform.is('mobile')) {
        this.nativeStorage.getItem('images').then(
          data => {
            this.images = data;
          },
          error => {
            this.nativeStorage.setItem('images', []).then(() => console.log('stored item'));
          }
        );
      }
      else {
        this.nativeStorage.getItem('browserImages').then(
          data => {
            this.imgURL = data;
          },
          error => {
            this.nativeStorage.setItem('browserImages', []).then(() => console.log('stored item'));
          }
        );
      }
    })
    
  }

  selectImage(id: number) {
    let self = this;
    if(!this.platform.is('core')) {
      var options: ImagePickerOptions = {
        maximumImagesCount: 1
      };

      window.imagePicker.getPictures(
        function(results) {
          self.images[id] = window.Ionic.WebView.convertFileSrc(results[0]);
          self.nativeStorage.setItem('images', self.images).then(() => console.log('stored item'));
        }, function (error) {
            console.log('Error: ' + error);
        }, 
        options
      );
    }
    else {

    }
  }

  verDetalles(pokemon: IPokemon) {
    if(this.platform.is('mobile')) {
      this.navCtrl.push(DetallesPage, {pokemon: pokemon, image: this.images[pokemon.id]});
    }
    else {
      this.navCtrl.push(DetallesPage, {pokemon: pokemon, image: this.imgURL[pokemon.id]});
    }
  }

  preview(files, id) {
    if (files.length === 0)
      return;
 
    var mimeType = files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      return;
    }
 
    var reader = new FileReader();
    this.imagePath = files;
    reader.readAsDataURL(files[0]); 
    reader.onload = (_event) => {
      this.imgURL[id] = reader.result;
      this.nativeStorage.setItem('browserImages', this.imgURL).then(() => console.log('stored item'));
    }
  }
}



interface IImage {
  id: number,
  url: string,
  urlBrowser: string | ArrayBuffer
}

