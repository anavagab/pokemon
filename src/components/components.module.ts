import { NgModule } from '@angular/core';
import { ListarPokemonsComponent } from './listar-pokemons/listar-pokemons';
@NgModule({
	declarations: [ListarPokemonsComponent],
	imports: [],
	exports: [ListarPokemonsComponent]
})
export class ComponentsModule {}
