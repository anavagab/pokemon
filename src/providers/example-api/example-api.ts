import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { HTTP } from '@ionic-native/http/ngx';
import { Platform } from 'ionic-angular';

declare var cordova: any;

/*
  Generated class for the ExampleApiProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class ExampleApiProvider {
  data: any;
  pokemon: any;

  constructor(public http2: HttpClient, public http: HTTP, public platform: Platform) {
  }

  loadPokemons() {
    
    return new Promise(resolve => {
      this.http2.get('https://pokeapi.co/api/v2/pokemon')
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }

  loadPokemons2() {
    
    if(this.platform.is('mobile')) {
      return new Promise((resolve, reject) => {
        cordova.plugin.http.setRequestTimeout(60);
        cordova.plugin.http.get(encodeURI('https://pokeapi.co/api/v2/pokemon'), {}, {}, function(response) {
          try {
            resolve(JSON.parse(response.data));
          }
          catch(err) {
            reject();
          }
        }, function(response) {
          reject();
        })
      });
    }
    else {
      return this.loadPokemons();
    }
    
  }

  loadPokemonsDetails(url: any) {
    if(this.platform.is('mobile')) {
      return new Promise((resolve, reject) => {
        cordova.plugin.http.setRequestTimeout(100);
        cordova.plugin.http.get(encodeURI(url), {}, {}, function(response) {
          try {
            resolve(JSON.parse(response.data));
          }
          catch(err) {
            reject();
          }
        }, function(response) {
          reject();
        })
      });
    }
    else {
      return this.loadPokemonsDetails2(url);
    }
    
  }

  loadPokemonsDetails2(url: any) {
    return new Promise(resolve => {
      this.http2.get(url)
        .subscribe(data => {
          this.data = data;
          resolve(this.data);
        });
    });
  }
}
