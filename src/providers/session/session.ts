import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { NativeStorage } from '@ionic-native/native-storage';
import { Platform } from 'ionic-angular';

declare var navigator: any;

/*
  Generated class for the SessionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class SessionProvider {

  constructor(public http: HttpClient,
    public nativeStorage: NativeStorage,
    public platform: Platform) {
  }

  getLanguage() {
    let self = this;
    return new Promise((resolve, reject) => {
      this.nativeStorage.getItem('lang').then(
        data => {
          resolve(data.lang);
        },
        error => {
          if(!self.platform.is('core')) {
            navigator.globalization.getPreferredLanguage(
              function(language) {
                if(language.value.startsWith('en')) {
                  resolve('en');
                }
                else {
                  resolve('es');
                }
              },
              function() {
                resolve('es');
              }
            );
          }
        }
      ),
      function(response) {
        if(!self.platform.is('core')) {
          navigator.globalization.getPreferredLanguage(
            function(language) {
              if(language.value.startsWith('en')) {
                resolve('en');
              }
              else {
                resolve('es');
              }
            },
            function() {
              resolve('es');
            }
          );
        }
      }
    })
  }

  setLanguage(lang: string) {
    this.nativeStorage.setItem('lang', {lang: lang}).then(() => console.log('stored language'));
  }
}